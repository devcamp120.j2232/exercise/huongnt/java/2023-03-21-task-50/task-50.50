import java.util.*;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {

        // 1:Count Frequency of Characters in a String
        String str = "DCresource: JavaScript Exercises";
        int[] freq = new int[str.length()];
        System.out.println("The entered string is " + str);
        // Convert the given string into character array
        char str1[] = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {
            freq[i] = 1;
            for (int j = i + 1; j < str.length(); j++) {
                if (str1[i] == str1[j]) {
                    freq[i]++;

                    // Set str1[j] to 0 to avoid printing visited character
                    str1[j] = '0';
                }
            }

        }
        // Displays the characters and their corresponding frequency
        System.out.println("1.Frequencies of the characters in the string are as below: ");
        System.out.println("1. Characters  frequencies");
        for (int i = 0; i < freq.length; i++) {
            if (str1[i] != ' ' && str1[i] != '0')
                System.out.println(str1[i] + "              " + freq[i]);
        }

        // 1:Count Frequency of Specific Characters in a String
        String searchText = "DCresource: JavaScript Exercises";
        String targetWord = "e";
        String[] words = searchText.replaceAll("\\p{Punct}", "").split(" ");

        int wordCount = 0;
        for (int i = 0; i < words.length; i++)
            if (words[i].equals(targetWord))
                wordCount++;
        System.out.println("1. Characters  frequencies of e: " + wordCount);

        // 2 Loại bỏ các ký tự trắng ở đầu và cuối chuỗi

        String str2 = " dcresource ";
        // Print the modified string
        System.out.print("2: After trim first and last space :" + str2.trim());

        // 3 Loại bỏ chính xác chuỗi con ra khỏi chuỗi cho trước
        String str3, word3;
        Scanner scan3 = new Scanner(System.in);
        str3 = "The quick brown fox jumps over the lazy dog";
        word3 = "the"; // Enter the Word to Remove
        str3 = str3.replaceAll(word3, "");
        System.out.println("\n3. The new string after remove the is: " + str3);

        // 4. Kiểm tra chuỗi sau có phải là kết thúc của chuỗi trước hay không
        String str4input1 = "JS PHP PYTHON";
        String str4last = str4input1.substring(7);
        String str4input2 = "PYTHON";
        String str4input3 = "python";
        // hàm equal so sánh 2 chuỗi giống nhau (có so sánh viết thường viết hoa)
        System.out.println("\n4. Last words Equal first word: " + str4last.equals(str4input2));
        // hàm equal so sánh 2 chuỗi giống nhau (k so sánh viết thường viết hoa)
        System.out.println("\n4. Last words Equal first word: " + str4last.equalsIgnoreCase(str4input3));

        // 5 So sánh 2 chuỗi có giống nhau hay không
        String str5input1 = "abcd";
        String str5input2 = "AbcD";
        String str5input3 = "ABCD";
        String str5input4 = "Abce";
        // hàm equal so sánh 2 chuỗi giống nhau (k so sánh viết thường viết hoa)
        System.out.println("\n5. Equal ignore case: " + str5input1.equalsIgnoreCase(str5input2));
        System.out.println("\n5. Equal ignore case: " + str5input3.equalsIgnoreCase(str5input4));

        // 6 Kiểm tra ký tự thứ n của chuỗi có phải viết hoa hay không
        String str6input1 = "Js STRING EXERCISES";
        char char6index1 = str6input1.charAt(1);
        char char6index3 = str6input1.charAt(3);
        System.out.println("6 Check whether a character is in Uppercase or not: " + Character.isUpperCase(char6index1));
        System.out.println("6 Check whether a character is in Uppercase or not: " + Character.isUpperCase(char6index3));

        // 7 Kiểm tra ký tự thứ n của chuỗi có phải viết thường hay không
        String str7input1 = "Js STRING EXERCISES";
        char char7index1 = str7input1.charAt(1);
        char char7index3 = str7input1.charAt(3);
        System.out.println("7 Check whether a character is in Lowercase or not: " + Character.isLowerCase(char7index1));
        System.out.println("7 Check whether a character is in Lowercase or not: " + Character.isLowerCase(char7index3));

        // 8. Kiểm tra chuỗi sau có phải là kết thúc của chuỗi trước hay không
        String str8input1 = "js string exercises";
        String str8last = str8input1.substring(0, 2);
        String str8input2 = "js";
        String str8input3 = "JS";
        // hàm equal so sánh 2 chuỗi giống nhau (có so sánh viết thường viết hoa)
        System.out.println("\n8. First word Equal first word: " + str8last.equals(str8input2));
        // hàm equal so sánh 2 chuỗi giống nhau (k so sánh viết thường viết hoa)
        System.out.println("\n8. First word Equal first word: " + str8last.equalsIgnoreCase(str8input3));

        // 9 Kiểm tra chuỗi đã cho có phải chuỗi rỗng hay không
        String str9input1 = "abc";

        if (str9input1 == null || str9input1.length() == 0)
            System.out.println("9. This string abc is null or empty.");
        else
            System.out.println("9. This string abc is neither null nor empty");

        String str9input2 = "";

        if (str9input2 == null || str9input2.length() == 0)
            System.out.println("9. This string < > is null or empty.");
        else
            System.out.println("9. This string < > is neither null nor empty");

        // 10 Đảo ngược chuỗi
        String str10 = "AaBbc", nstr = "";
        char ch;

        for (int i = 0; i < str10.length(); i++) {
            ch = str10.charAt(i); // extracts each character
            nstr = ch + nstr; // adds each character in front of the existing string
        }
        System.out.println("10. Reversed word: " + nstr);
    }

}
